# Restic server

Ce service est un serveur de backups restic. Il implémente l'API Rest de Restic afin d'effectuer des backups distants, il est à déployer sur chaque machine destinée à héberger des backups.

# Premier lancement

Il faut cloner ce dépôt, définir le sous domaine dans la variable `SUBDOMAIN` du fichier `.env` et lancer le service avec `docker compose up -d`.

Il faudra ensuite créer un certificat dans le dépôt traefik (voir la doc)

# Mettre à jour

Il faut changer le tag de l'image dans le fichier `docker-compose.yml` et lancer la nouvelle version de l'image avec `docker compose up -d`.
